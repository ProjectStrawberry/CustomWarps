package com.gmail.jd4656.customwarps;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventListeners implements Listener {
    private CustomWarps plugin;

    EventListeners(CustomWarps p) {
        plugin = p;
    }

    @EventHandler
    public void PlayerMoveEvent(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        Location from = event.getFrom();
        Location to = event.getTo();
        if (plugin.pendingWarps.containsKey(player.getName()) && (from.getBlockX() != to.getBlockX() || from.getBlockY() != to.getBlockY() || from.getBlockZ() != to.getBlockZ())) {
            Bukkit.getServer().getScheduler().cancelTask(plugin.pendingWarps.get(player.getName()));
            plugin.pendingWarps.remove(player.getName());
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You moved! Your warp has been cancelled.");
        }
    }

    @EventHandler
    public void PlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (plugin.pendingWarps.containsKey(player.getName())) {
            Bukkit.getServer().getScheduler().cancelTask(plugin.pendingWarps.get(player.getName()));
            plugin.pendingWarps.remove(player.getName());
        }
    }

    @EventHandler
    public void EntityDamageEvent(EntityDamageEvent event) {
        if (event.getEntityType() != EntityType.PLAYER) return;
        Player player = (Player) event.getEntity();

        if (plugin.pendingWarps.containsKey(player.getName())) {
            Bukkit.getServer().getScheduler().cancelTask(plugin.pendingWarps.get(player.getName()));
            plugin.pendingWarps.remove(player.getName());
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Your warp has been cancelled due to taking damage.");
        }
    }
}
