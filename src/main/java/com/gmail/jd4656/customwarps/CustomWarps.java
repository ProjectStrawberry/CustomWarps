package com.gmail.jd4656.customwarps;

import com.gmail.jd4656.customwarps.Commands.*;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class CustomWarps extends JavaPlugin {
    private CustomWarps plugin;
    private File dbFile;
    private Connection conn = null;
    public Map<String, Integer> pendingWarps = new HashMap<>();

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        dbFile = new File(plugin.getDataFolder(), "warps.db");
        if (!dbFile.exists()) {
            try {
                dbFile.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().log(Level.SEVERE, "File write error: warps.db");
            }
        }

        try {
            String sql;
            Statement stmt;
            ResultSet rs;

            Connection c = this.getConnection();

            stmt = c.createStatement();
            sql = "CREATE TABLE IF NOT EXISTS warps (parent TEXT, categoryName TEXT, warpName TEXT, displayName TEXT, displayItem TEXT, createdBy TEXT, world TEXT, x INTEGER, y INTEGER, z INTEGER, yaw INTEGER, pitch INTEGER);";
            stmt.executeUpdate(sql);

            stmt = c.createStatement();
            sql = "SELECT count(*) FROM warps;";
            rs = stmt.executeQuery(sql);

            if (rs.getInt("count(*)") < 1) {
                stmt = c.createStatement();
                sql = "INSERT INTO warps (parent, categoryName, displayName, createdBy) VALUES ('root', 'main_warps', 'Main_Warps', 'CustomWarps');";
                stmt.executeUpdate(sql);

                stmt = c.createStatement();
                sql = "INSERT INTO warps (parent, categoryName, displayName, createdBy) VALUES ('root', 'market_warps', 'Market_Warps', 'CustomWarps');";
                stmt.executeUpdate(sql);

                stmt = c.createStatement();
                sql = "INSERT INTO warps (parent, categoryName, displayName, createdBy) VALUES ('root', 'other_warps', 'Other_Warps', 'CustomWarps');";
                stmt.executeUpdate(sql);

                stmt = c.createStatement();
                sql = "INSERT INTO warps (parent, categoryName, displayName, createdBy) VALUES ('root', 'hidden', 'Hidden', 'CustomWarps');";
                stmt.executeUpdate(sql);
            }

            stmt = c.createStatement();
            rs = stmt.executeQuery("PRAGMA user_version");

            int dbVersion = rs.getInt("user_version");

            /*if (dbVersion == 0) {

            }*/

            stmt.close();
        } catch (Exception e) {
            getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            getLogger().warning("CustomWarps failed to load.");
            return;
        }

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);

        this.getCommand("newwarpcategory").setExecutor(new newwarpcategory(this));
        this.getCommand("setwarp").setExecutor(new setwarp(this));
        this.getCommand("delwarp").setExecutor(new delwarp(this));
        this.getCommand("renamewarp").setExecutor(new renamewarp(this));
        this.getCommand("replacewarp").setExecutor(new replacewarp(this));
        this.getCommand("warpinfo").setExecutor(new warpinfo(this));
        this.getCommand("setwarpicon").setExecutor(new setwarpicon(this));
        this.getCommand("grantwarp").setExecutor(new grantwarp(this));
        this.getCommand("warp").setExecutor(new warp(this));

        getLogger().info("CustomWarps loaded.");
    }

    @Override
    public void onDisable() {
        if (conn != null) {
            try {
                conn.close();
                conn = null;
            } catch (Exception ignored) {}
        }
        getLogger().info("CustomWarps unloaded.");
    }

     public Connection getConnection() throws Exception {
        if (conn == null ) {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + this.dbFile);
        }
        return conn;
    }

    // checks if a warp or category exists
    public int checkWarp(String warp) {
        try {
            Connection c = plugin.getConnection();
            c.setAutoCommit(true);
            String sql;
            PreparedStatement pstmt;
            ResultSet rs;

            sql = "SELECT * FROM warps WHERE categoryName=? OR warpName=?";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, warp);
            pstmt.setString(2, warp);
            rs = pstmt.executeQuery();

            String categoryName = null;
            String warpName = null;

            try {
                categoryName = rs.getString("categoryName");
            } catch (SQLException ignored) {}
            try {
                warpName = rs.getString("warpName");
            } catch (SQLException ignored) {}

            if (categoryName != null && categoryName.equals(warp)) {
                pstmt.close();
                return 1;
            } else if (warpName != null && warpName.equals(warp)) {
                pstmt.close();
                return 2;
            }

            pstmt.close();
            return 0;
        } catch (Exception e) {
            plugin.getLogger().severe("checkWarp: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            return 0;
        }
    }

     public String cleanSpaces(String str) {
        if (str.contains("_")) {
            str = String.join(" ", str.split("_"));
        }
        return str;
    }

     public String cleanColours(String str) {
        return str.replaceAll("§[0-9a-z]", "");
    }

     public String getPermission(String warp, String type) {
        StringBuilder permission = new StringBuilder();
        String parent = "";
        try {
            String sql;
            PreparedStatement pstmt;
            ResultSet rs;

            Connection c = plugin.getConnection();
            c.setAutoCommit(true);

            sql = "SELECT * FROM warps WHERE warpName=? OR categoryName=?";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, warp);
            pstmt.setString(2, warp);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                String categoryName = null;
                String warpName = null;

                try {
                    categoryName = rs.getString("categoryName");
                } catch (SQLException ignored) {}
                try {
                    warpName = rs.getString("warpName");
                } catch (SQLException ignored) {}

                if (warp.equals(warpName)) {
                    permission.append(rs.getString("warpName"));
                    parent = rs.getString("parent");
                }
                if (warp.equals(categoryName)) {
                    permission.append(rs.getString("categoryName"));
                    parent = rs.getString("parent");
                }
            }

            while (!parent.equals("root")) {
                sql = "SELECT * FROM warps WHERE categoryName=?";
                pstmt = c.prepareStatement(sql);
                pstmt.setString(1, parent);
                rs = pstmt.executeQuery();

                permission.insert(0, rs.getString("categoryName") + ".");
                parent = rs.getString("parent");
            }

            pstmt.close();

            permission.insert(0, "customwarps." + type + ".");
            if (permission.toString().contains("_")) {
                permission = new StringBuilder(String.join("_", permission.toString().split("_")));
            }
            return permission.toString();
        } catch (Exception e) {
            plugin.getLogger().severe("getPermission: " + e.getClass().getName() + ": " + e.getMessage());
            return "customwarps." + type + "." + warp;
        }
    }
}
