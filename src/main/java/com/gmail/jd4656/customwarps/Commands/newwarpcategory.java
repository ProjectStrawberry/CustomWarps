package com.gmail.jd4656.customwarps.Commands;

import com.gmail.jd4656.customwarps.CustomWarps;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class newwarpcategory implements CommandExecutor {
    private CustomWarps plugin;

    public newwarpcategory(CustomWarps p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command does not work from the console.");
            return true;
        }
        if (args.length != 2) return false;

        if (!sender.hasPermission("customwarps.manage")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to create warps.");
            return true;
        }

        String warpParent = args[0].toLowerCase();
        String warpCategory = args[1].toLowerCase();
        String displayName = args[1];

        if (warpParent.equals("root")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You can't create sub categories under the root category.");
            return true;
        }

        if (plugin.checkWarp(warpParent) != 1) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That parent category does not exist.");
            return true;
        }

        if (plugin.checkWarp(warpCategory) != 0) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That category already exists.");
            return true;
        }

        if (!sender.hasPermission(plugin.getPermission(warpParent, "manage"))) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to create sub categories under this category.");
            return true;
        }

        try {
            Connection c = plugin.getConnection();
            String sql;
            PreparedStatement pstmt;
            ResultSet rs;
            boolean rootCategory = false;

            sql = "SELECT count(*) FROM warps WHERE categoryName=? AND parent=\"root\"";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, warpParent);
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") > 0) {
                rootCategory = true;
            }

            sql = "SELECT count(*) FROM warps WHERE parent=?";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, warpParent);
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") >= 9 && rootCategory) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You can't add more than 9 warps under a main category.");
                pstmt.close();
                return true;
            }

            if (rs.getInt("count(*)") >= 54) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You can't add more than 54 warps under a category.");
                pstmt.close();
                return true;
            }

            sql = "INSERT INTO warps (parent, categoryName, displayName, createdBy) VALUES (?, ?, ?, ?);";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, warpParent);
            pstmt.setString(2, warpCategory);
            pstmt.setString(3, displayName);
            pstmt.setString(4, sender.getName());
            pstmt.executeUpdate();

            sender.sendMessage(ChatColor.GOLD + "A sub category under " + ChatColor.RED + warpParent + ChatColor.GOLD + " has been created.");
            pstmt.close();
            return true;
        } catch (Exception e) {
            plugin.getLogger().severe("/newwarpcategory: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
            return true;
        }
    }
}