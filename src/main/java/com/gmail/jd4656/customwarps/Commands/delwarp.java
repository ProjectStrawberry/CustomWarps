package com.gmail.jd4656.customwarps.Commands;

import com.gmail.jd4656.customwarps.CustomWarps;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class delwarp implements CommandExecutor {
    private CustomWarps plugin;
    public delwarp(CustomWarps p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        if (args.length < 1) return false;

        if (!sender.hasPermission("customwarps.manage")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to delete warps.");
            return true;
        }

        String warpName = args[0].toLowerCase();

        int warpExists = plugin.checkWarp(warpName);

        if (warpExists == 0) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That warp does not exist.");
            return true;
        }

        try {
            String sql;
            PreparedStatement pstmt;
            ResultSet rs;

            Connection c = plugin.getConnection();

            if (warpExists == 2) {
                if (!sender.hasPermission(plugin.getPermission(warpName, "manage"))) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to delete this warp.");
                    return true;
                }
                sql = "DELETE FROM warps WHERE warpName=?";
                pstmt = c.prepareStatement(sql);
                pstmt.setString(1, warpName);
                pstmt.executeUpdate();
                pstmt.close();
                sender.sendMessage(ChatColor.GOLD + "That warp has been deleted.");
                return true;
            }

            sql = "SELECT count(*) FROM warps WHERE parent=?";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, warpName);
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                if (!sender.hasPermission(plugin.getPermission(warpName, "manage"))) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to delete this category.");
                    pstmt.close();
                    return true;
                }

                sql = "DELETE FROM warps WHERE categoryName=?";
                pstmt = c.prepareStatement(sql);
                pstmt.setString(1, warpName);
                pstmt.executeUpdate();
                pstmt.close();
                sender.sendMessage(ChatColor.GOLD + "That category has been deleted.");
                return true;
            } else {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You can't delete a category unless it's empty.");
                pstmt.close();
                return true;
            }
        } catch (Exception e) {
            plugin.getLogger().severe("/delwarp: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
            return true;
        }
    }
}
