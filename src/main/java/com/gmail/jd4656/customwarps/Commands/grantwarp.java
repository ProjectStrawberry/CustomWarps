package com.gmail.jd4656.customwarps.Commands;

import com.gmail.jd4656.customwarps.CustomWarps;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class grantwarp implements TabExecutor {
    private CustomWarps plugin;

    public grantwarp(CustomWarps p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("customwarps.grant")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        if (args.length < 2) {
            sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /grantwarp <username> <warp/category> - Gives a user the permission to manage a warp.");
            return true;
        }

        String warp = args[1];
        if (plugin.checkWarp(warp) == 0) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That warp does not exist.");
            return true;
        }

        Bukkit.dispatchCommand(sender, "lp user " + args[0] + " permission set " + plugin.getPermission(warp, "manage") + " world=worldtowny");

        return true;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1) return null;
        if (args.length == 2) tabComplete.add("<warp / category>");
        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
