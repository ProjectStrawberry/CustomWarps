package com.gmail.jd4656.customwarps.Commands;

import com.gmail.jd4656.customwarps.CustomWarps;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class renamewarp implements CommandExecutor {
    private CustomWarps plugin;

    public renamewarp(CustomWarps p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command does not work from the console.");
            return true;
        }
        if (args.length != 2) return false;

        if (!sender.hasPermission("customwarps.rename")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to rename warps.");
            return true;
        }

        String warp = args[0].toLowerCase();
        String renameTo = args[1].toLowerCase();
        String displayName = args[1];

        if (plugin.checkWarp(renameTo) != 0) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is already a warp named " + ChatColor.RED + renameTo);
            return true;
        }

        int warpExists = plugin.checkWarp(warp);

        if (warpExists > 0) {
            if (!sender.hasPermission(plugin.getPermission(warp, "rename"))) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to rename this warp.");
                return true;
            }

            try {
                Connection c = plugin.getConnection();
                String sql;
                PreparedStatement pstmt;

                String type = "categoryName";
                if (warpExists == 2) type = "warpName";

                sql = "UPDATE warps SET " + type + "=?, displayName=? WHERE " + type + "=?;";
                pstmt = c.prepareStatement(sql);
                pstmt.setString(1, renameTo);
                pstmt.setString(2, displayName);
                pstmt.setString(3, warp);
                pstmt.executeUpdate();

                if (warpExists == 1) {
                    sql = "UPDATE warps SET parent=? WHERE parent=?";
                    pstmt = c.prepareStatement(sql);
                    pstmt.setString(1, renameTo);
                    pstmt.setString(2, warp);
                    pstmt.executeUpdate();
                }

                pstmt.close();

                sender.sendMessage(ChatColor.GOLD + "The " + (warpExists == 1 ? "category " : "warp ") + ChatColor.RED + warp + ChatColor.GOLD + " has been renamed to " + ChatColor.RED + displayName);
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/renamewarp: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That warp does not exist.");
        return true;
    }
}
