package com.gmail.jd4656.customwarps.Commands;

import com.gmail.jd4656.customwarps.CustomWarps;
import com.gmail.jd4656.InventoryManager.InventoryClickHandler;
import com.gmail.jd4656.InventoryManager.InventoryManager;
import io.papermc.lib.PaperLib;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;


public class warp implements CommandExecutor {
    private Map<UUID, Long> warpCooldowns = new HashMap<>();
    private CustomWarps plugin;

    public warp(CustomWarps p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        if (!sender.hasPermission("customwarps.use")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        Player player = (Player) sender;
        if (args.length == 0) {
            try {
                String sql;
                PreparedStatement pstmt;
                ResultSet rs;
                //LinkedList<String> categories = new LinkedList<>();
                Map<String, String> categories = new LinkedHashMap<>();

                Connection c = plugin.getConnection();

                sql = "SELECT * FROM warps WHERE parent=\"root\" AND categoryName not null;";
                pstmt = c.prepareStatement(sql);
                rs = pstmt.executeQuery();

                while (rs.next()) {
                     categories.put(rs.getString("displayName"), rs.getString("displayItem"));
                }

                if (categories.size() < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "No warps have been added.");
                    rs.close();
                    pstmt.close();
                    return true;
                }

                InventoryManager manager = new InventoryManager(ChatColor.GOLD + ChatColor.UNDERLINE.toString() + "Warp Categories", 54, plugin);

                int invPos = 4;
                double warpPos;
                Material concreteType = Material.LIGHT_BLUE_CONCRETE;

                int count = 0;

                for (Map.Entry<String, String> categoryEntry : categories.entrySet()) {
                    String colour = ChatColor.DARK_AQUA + ChatColor.UNDERLINE.toString();
                    if (count == 1) colour = ChatColor.DARK_GREEN + ChatColor.UNDERLINE.toString();
                    if (count == 2) colour = ChatColor.DARK_RED + ChatColor.UNDERLINE.toString();
                    String displayName = categoryEntry.getKey();
                    if (displayName.toLowerCase().equals("hidden")) {
                        count++;
                        continue;
                    }
                    String itemStr = categoryEntry.getValue();
                    ItemStack item = new ItemStack(Material.ANVIL, 1);
                    if (itemStr != null) {
                        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(itemStr));
                        BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

                        item = (ItemStack) dataInput.readObject();
                        dataInput.close();
                    }
                    ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName(colour + plugin.cleanSpaces(displayName));
                    item.setItemMeta(meta);

                    manager.withItem(invPos, item);

                    Map<String, String> curResults = new LinkedHashMap<>();

                    if (count < 2) {
                        warpPos = invPos + 5;
                        pstmt = c.prepareStatement("SELECT * FROM warps WHERE parent=? AND warpName not null;");
                        pstmt.setString(1, displayName.toLowerCase());
                        rs = pstmt.executeQuery();

                        while (rs.next()) {
                            if (sender.hasPermission(plugin.getPermission(rs.getString("warpName"), "use"))) curResults.put(rs.getString("displayName"), rs.getString("displayItem"));
                        }

                        pstmt = c.prepareStatement("SELECT * FROM warps WHERE parent=? AND categoryName not null;");
                        pstmt.setString(1, displayName.toLowerCase());
                        rs = pstmt.executeQuery();

                        while (rs.next()) {
                            if (sender.hasPermission(plugin.getPermission(rs.getString("categoryName"), "use"))) curResults.put(rs.getString("displayName"), rs.getString("displayItem"));
                        }

                        warpPos += Math.floor((9 - curResults.size()) / 2);

                        for (Map.Entry<String, String> entry : curResults.entrySet()) {
                            String curName = entry.getKey();
                            String itemText = entry.getValue();
                            ItemStack curItem = new ItemStack(concreteType, 1);

                            if (itemText != null) {
                                ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(itemText));
                                BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

                                curItem = (ItemStack) dataInput.readObject();
                                dataInput.close();
                            }
                            ItemMeta curMeta = curItem.getItemMeta();

                            curMeta.setDisplayName((concreteType.equals(Material.LIGHT_BLUE_CONCRETE) ? ChatColor.AQUA : ChatColor.GREEN) + ChatColor.UNDERLINE.toString() + plugin.cleanSpaces(curName));
                            curItem.setItemMeta(curMeta);
                            manager.withItem((int) warpPos, curItem);
                            warpPos++;
                        }
                        if (concreteType.equals(Material.LIGHT_BLUE_CONCRETE)) concreteType = Material.LIME_CONCRETE;
                    }

                    if (count == 2) {
                        concreteType = Material.RED_CONCRETE;
                        warpPos = invPos + 5;
                        sql = "SELECT * FROM warps WHERE parent=? AND categoryName not null;";
                        pstmt = c.prepareStatement(sql);
                        pstmt.setString(1, displayName.toLowerCase());
                        rs = pstmt.executeQuery();

                        while (rs.next()) {
                            if (sender.hasPermission(plugin.getPermission(rs.getString("categoryName"), "use"))) curResults.put(rs.getString("displayName"), rs.getString("displayItem"));
                        }

                        warpPos += Math.floor((9 - curResults.size()) / 2);

                        for (Map.Entry<String, String> entry : curResults.entrySet()) {
                            String curName = entry.getKey();
                            String itemText = entry.getValue();
                            ItemStack curItem = new ItemStack(concreteType, 1);

                            if (itemText != null) {
                                ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(itemText));
                                BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

                                curItem = (ItemStack) dataInput.readObject();
                                dataInput.close();
                            }
                            ItemMeta curMeta = curItem.getItemMeta();

                            curMeta.setDisplayName(ChatColor.RED + ChatColor.UNDERLINE.toString() + plugin.cleanSpaces(curName));
                            curItem.setItemMeta(curMeta);
                            manager.withItem((int) warpPos, curItem);
                            warpPos++;
                        }
                    }

                    invPos += 18;
                    count++;
                }

                rs.close();
                pstmt.close();

                manager.withEventHandler(new InventoryClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent event) {
                        Player player = (Player) event.getWhoClicked();
                        ItemStack clicked = event.getCurrentItem();
                        if (clicked.getType().equals(Material.AIR)) {
                            event.setCancelled(true);
                            return;
                        }
                        ItemMeta meta = clicked.getItemMeta();
                        event.setCancelled(true);
                        String name = meta.getDisplayName();

                        if (name == null) return;

                        try {
                            String sql;
                            PreparedStatement pstmt;
                            ResultSet rs;

                            Connection c = plugin.getConnection();

                            String warpName = name;
                            if (warpName.contains(" ")) {
                                warpName = String.join("_", warpName.split(" "));
                            }

                            int warpType = plugin.checkWarp(ChatColor.stripColor(warpName).toLowerCase());

                            if (warpType == 1) {
                                InventoryManager manager = new InventoryManager(ChatColor.DARK_RED + ChatColor.UNDERLINE.toString() + plugin.cleanSpaces(name), 54, plugin);

                                if (name.contains(" ")) {
                                    name = String.join("_", name.split(" "));
                                }

                                name = plugin.cleanColours(name);

                                sql = "SELECT * FROM warps WHERE parent=? AND categoryName not null;";
                                pstmt = c.prepareStatement(sql);
                                pstmt.setString(1, name.toLowerCase());
                                rs = pstmt.executeQuery();

                                int pos = 0;


                                while (rs.next()) {
                                    String curName = rs.getString("displayName");
                                    String itemText = rs.getString("displayItem");
                                    if (curName.contains(" ")) {
                                        curName = String.join(" ", curName.split("_"));
                                    }
                                    if (!player.hasPermission(plugin.getPermission(rs.getString("categoryName"), "use"))) continue;
                                    ItemStack item = new ItemStack(Material.RED_CONCRETE, 1);
                                    if (itemText != null) {
                                        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(itemText));
                                        BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

                                        item = (ItemStack) dataInput.readObject();
                                        dataInput.close();
                                    }
                                    ItemMeta curMeta = item.getItemMeta();
                                    curMeta.setDisplayName(ChatColor.RED + ChatColor.UNDERLINE.toString() + plugin.cleanSpaces(curName));
                                    item.setItemMeta(curMeta);
                                    manager.withItem(pos, item);
                                    pos++;
                                }

                                sql = "SELECT * FROM warps WHERE parent=? AND warpName not null;";
                                pstmt = c.prepareStatement(sql);
                                pstmt.setString(1, name.toLowerCase());
                                rs = pstmt.executeQuery();

                                while (rs.next()) {
                                    String curName = rs.getString("displayName");
                                    String itemText = rs.getString("displayItem");
                                    if (!player.hasPermission(plugin.getPermission(rs.getString("warpName"), "use"))) continue;
                                    if (curName.contains(" ")) {
                                        curName = String.join(" ", curName.split("_"));
                                    }
                                    ItemStack item = new ItemStack(Material.LIGHT_BLUE_CONCRETE, 1);
                                    if (itemText != null) {
                                        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(itemText));
                                        BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

                                        item = (ItemStack) dataInput.readObject();
                                        dataInput.close();
                                    }
                                    ItemMeta curMeta = item.getItemMeta();
                                    curMeta.setDisplayName(ChatColor.AQUA + ChatColor.UNDERLINE.toString() + plugin.cleanSpaces(curName));
                                    item.setItemMeta(curMeta);
                                    manager.withItem(pos, item);
                                    pos++;
                                }

                                rs.close();
                                pstmt.close();

                                manager.withEventHandler(this);
                                manager.show(player);
                            }

                            if (warpType == 2) {
                                if (name.contains(" ")) {
                                    name = String.join("_", name.split(" "));
                                }
                                name = plugin.cleanColours(name);
                                player.closeInventory();
                                player.chat("/warp " + name.toLowerCase());
                            }
                        } catch (Exception e) {
                            plugin.getLogger().severe("/warp: " + e.getClass().getName() + ": " + e.getMessage());
                            e.printStackTrace();
                            player.sendMessage("Error querying database.");
                        }
                    }
                });

                manager.show(player);
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/warp: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        if (plugin.pendingWarps.containsKey(sender.getName())) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You already have a pending warp.");
            return true;
        }

        String targetWarp = args[0].toLowerCase();
        String displayName = "";

        // hardcoded /warp resource alias
        /*if (targetWarp.equals("resource")) {
            player.chat("/randomtp resource");
            return true;
        }*/

        try {
            String sql;
            PreparedStatement pstmt;
            ResultSet rs;
            boolean found = false;
            World targetWorld;
            Location targetLocation = player.getLocation();

            Connection c = plugin.getConnection();

            sql = "SELECT * FROM warps WHERE warpName=?";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, targetWarp);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                if (targetWarp.equals(rs.getString("warpName"))) {
                    displayName = plugin.cleanSpaces(rs.getString("displayName"));
                    found = true;
                    targetWorld = Bukkit.getWorld(rs.getString("world"));
                    targetLocation = new Location(targetWorld, rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs.getFloat("yaw"), rs.getFloat("pitch"));
                }
            }

            rs.close();
            pstmt.close();

            if (!found) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That warp does not exist.");
                return true;
            }

            if (!sender.hasPermission(plugin.getPermission(targetWarp, "use"))) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this warp.");
                return true;
            }

            int seconds = plugin.getConfig().getInt("delay");
            int cooldown = plugin.getConfig().getInt("cooldown");
            long ticks = seconds * 20;

            if (warpCooldowns.containsKey(sender.getName())) {
                Date date = new Date();
                long ms = date.getTime();
                long cd = warpCooldowns.get(sender.getName());
                long remainingTime = (ms - cd);
                long remainingTimeSeconds = Math.round(remainingTime / 1000);
                if (remainingTimeSeconds < cooldown) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Time before next teleport: " + ChatColor.RED + (cooldown - remainingTimeSeconds) + ((cooldown - remainingTimeSeconds) == 1 ? " second." : " seconds."));
                    return true;
                }
                warpCooldowns.remove(sender.getName());
            }

            if (seconds < 1 || sender.hasPermission(plugin.getPermission(targetWarp, "override"))) {
                sender.sendMessage(ChatColor.GOLD + "Warping to " + ChatColor.RED + displayName + ".");
                PaperLib.teleportAsync(player, targetLocation).thenAccept(result -> {
                    if (result) {
                        player.sendMessage(ChatColor.GOLD + "Warped!");
                        if (cooldown > 0 && !sender.hasPermission(plugin.getPermission(targetWarp, "override"))) warpCooldowns.put(player.getUniqueId(), System.currentTimeMillis());
                    } else {
                        player.sendMessage(ChatColor.RED + "Failed to warp.");
                    }
                });
            } else {
                Location finalTargetLocation = targetLocation;
                int taskId = Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                    if (!plugin.pendingWarps.containsKey(sender.getName())) return;
                    plugin.pendingWarps.remove(sender.getName());
                    sender.sendMessage(ChatColor.GOLD + "Warping...");
                    PaperLib.teleportAsync(player, finalTargetLocation).thenAccept(result -> {
                        if (result) {
                            player.sendMessage(ChatColor.GOLD + "Warped!");
                            if (cooldown > 0 && !sender.hasPermission(plugin.getPermission(targetWarp, "override"))) warpCooldowns.put(player.getUniqueId(), System.currentTimeMillis());
                        } else {
                            player.sendMessage(ChatColor.RED + "Failed to warp.");
                        }
                    });
                }, ticks);
                plugin.pendingWarps.put(sender.getName(), taskId);
                sender.sendMessage(ChatColor.GOLD + "Warping to " + ChatColor.RED + displayName + ChatColor.GOLD + " in " + ChatColor.RED + seconds + " seconds" + ChatColor.GOLD + ", don't move!");
            }

            return true;
        } catch (Exception e) {
            plugin.getLogger().severe("/warp: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
            return true;
        }
    }
}