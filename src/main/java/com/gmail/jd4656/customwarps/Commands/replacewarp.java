package com.gmail.jd4656.customwarps.Commands;

import com.gmail.jd4656.customwarps.CustomWarps;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class replacewarp implements CommandExecutor {
    private CustomWarps plugin;

    public replacewarp(CustomWarps p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command does not work from the console.");
            return true;
        }
        if (args.length != 1) return false;

        if (!sender.hasPermission("customwarps.manage")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to edit warps.");
            return true;
        }

        String warp = args[0].toLowerCase();

        if (plugin.checkWarp(warp) == 2) {
            if (!sender.hasPermission(plugin.getPermission(warp, "manage"))) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to edit this warp.");
                return true;
            }

            Player player = (Player) sender;
            Location loc = player.getLocation();
            World world = player.getWorld();

            try {
                Connection c = plugin.getConnection();
                c.setAutoCommit(true);

                String sql = "UPDATE warps SET createdBy=?, world=?, x=?, y=?, z=?, yaw=?, pitch=? WHERE warpName=?;";
                PreparedStatement pstmt = c.prepareStatement(sql);
                pstmt.setString(1, sender.getName());
                pstmt.setString(2, world.getName());
                pstmt.setDouble(3, loc.getX());
                pstmt.setDouble(4, loc.getY());
                pstmt.setDouble(5, loc.getZ());
                pstmt.setFloat(6, loc.getYaw());
                pstmt.setFloat(7, loc.getPitch());
                pstmt.setString(8, warp);
                pstmt.executeUpdate();

                pstmt.close();

                sender.sendMessage(ChatColor.GOLD + "Your warp has been set.");

                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/replacewarp: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That warp does not exist.");
        return true;
    }
}
