package com.gmail.jd4656.customwarps.Commands;

import com.gmail.jd4656.customwarps.CustomWarps;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class setwarp implements CommandExecutor {
    private CustomWarps plugin;

    public setwarp(CustomWarps p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        if (args.length < 2) return false;

        if (!sender.hasPermission("customwarps.manage")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to create warps.");
            return true;
        }

        Player player = (Player) sender;

        String warpName = args[0].toLowerCase();
        String displayName = args[0];
        String warpCategory = args[1].toLowerCase();

        if (plugin.checkWarp(warpName) != 0) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A warp with that name already exists.");
            return true;
        }

        if (plugin.checkWarp(warpCategory) != 1) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That category does not exist.");
            return true;
        }

        if (!sender.hasPermission(plugin.getPermission(warpCategory, "manage"))) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to create warps under this category.");
            return true;
        }

        try {
            String sql;
            PreparedStatement pstmt;
            ResultSet rs;
            boolean rootCategory = false;

            Connection c = plugin.getConnection();

            Location loc = player.getLocation();
            World world = player.getWorld();

            sql = "SELECT count(*) FROM warps WHERE categoryName=? AND parent=\"root\"";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, warpCategory);
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") > 0) {
                rootCategory = true;
            }

            sql = "SELECT count(*) FROM warps WHERE parent=?";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, warpCategory);
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") >= 9 && rootCategory) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You can't add more than 9 warps under a main category.");
                pstmt.close();
                return true;
            }

            if (rs.getInt("count(*)") >= 54) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You can't add more than 54 warps under a category.");
                pstmt.close();
                return true;
            }

            sql = "INSERT INTO warps (parent, warpName, displayName, createdBy, world, x, y, z, yaw, pitch) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, warpCategory);
            pstmt.setString(2, warpName);
            pstmt.setString(3, displayName);
            pstmt.setString(4, sender.getName());
            pstmt.setString(5, world.getName());
            pstmt.setDouble(6, loc.getX());
            pstmt.setDouble(7, loc.getY());
            pstmt.setDouble(8, loc.getZ());
            pstmt.setFloat(9, loc.getYaw());
            pstmt.setFloat(10, loc.getPitch());
            pstmt.executeUpdate();

            pstmt.close();

            sender.sendMessage(ChatColor.GOLD + "You have added a warp.");
            return true;
        } catch (Exception e) {
            plugin.getLogger().severe("/setwarp: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
            return true;
        }
    }
}