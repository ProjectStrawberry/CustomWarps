package com.gmail.jd4656.customwarps.Commands;

import com.gmail.jd4656.customwarps.CustomWarps;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class warpinfo implements CommandExecutor {
    private CustomWarps plugin;

    public warpinfo(CustomWarps p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        if (!sender.hasPermission("customwarps.manage")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        if (args.length < 1) return false;

        String targetWarp = args[0].toLowerCase();
        int warpType = plugin.checkWarp(targetWarp);

        if (warpType == 0) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A warp with that name does not exist.");
            return true;
        }

        if (!sender.hasPermission(plugin.getPermission(targetWarp, "manage"))) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to get information on this " + (warpType == 1 ? "category." : "warp."));
            return true;
        }

        try {
            String sql;
            PreparedStatement pstmt;
            ResultSet rs;

            Connection c = plugin.getConnection();
            c.setAutoCommit(true);

            sql = "SELECT * FROM warps WHERE " + (warpType == 1 ? "categoryName" : "warpName") + "=?";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, targetWarp);
            rs = pstmt.executeQuery();

            sender.sendMessage(ChatColor.GOLD + "This " + (warpType == 1 ? "category" : "warp") + " was last edited by " + ChatColor.RED + rs.getString("createdBy"));

            pstmt.close();
            return true;
        } catch (Exception e) {
            plugin.getLogger().severe("/warpinfo: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
            return true;
        }
    }
}
