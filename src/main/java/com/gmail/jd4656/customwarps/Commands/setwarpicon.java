package com.gmail.jd4656.customwarps.Commands;

import com.gmail.jd4656.customwarps.CustomWarps;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

public class setwarpicon implements TabExecutor {
    private CustomWarps plugin;
    public setwarpicon(CustomWarps p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /setwarpicon <warp/category name> - Sets the icon for a warp to the item in your hand.");
            return true;
        }
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }
        if (!sender.hasPermission("customwarps.manage")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to edit warps.");
            return true;
        }

        Player player = (Player) sender;

        String warp = args[0].toLowerCase();
        int warpExists = plugin.checkWarp(warp);

        if (warpExists > 0) {
            if (!sender.hasPermission(plugin.getPermission(warp, "rename"))) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to rename this warp.");
                return true;
            }

            ItemStack item = player.getInventory().getItemInMainHand();
            if (item == null || item.getType().equals(Material.AIR)) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You're not holding an item.");
                return true;
            }

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt;

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

                dataOutput.writeObject(item);
                dataOutput.close();

                if (warpExists == 1) {
                    pstmt = c.prepareStatement("UPDATE warps SET displayItem=? WHERE categoryName=? AND warpName IS NULL");
                    pstmt.setString(1, Base64Coder.encodeLines(outputStream.toByteArray()));
                    pstmt.setString(2, warp);
                    pstmt.executeUpdate();
                    pstmt.close();
                }

                if (warpExists == 2) {
                    pstmt = c.prepareStatement("UPDATE warps SET displayItem=? WHERE warpName=?");
                    pstmt.setString(1, Base64Coder.encodeLines(outputStream.toByteArray()));
                    pstmt.setString(2, warp);
                    pstmt.executeUpdate();
                    pstmt.close();
                }

                sender.sendMessage(ChatColor.GOLD + "The icon for the " + (warpExists == 1 ? "category " : "warp ") + ChatColor.RED + warp + ChatColor.GOLD + " has been set to the item in your hand.");
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/setwarpicon: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That warp does not exist.");
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        return null;
    }
}
